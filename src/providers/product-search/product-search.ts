import { HttpClient,HttpParams,HttpHeaders  } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from "rxjs/operators";
import moment from'moment';

import { ProductCategory } from '../product-category/product-category';

/*
  Generated class for the ProductSearchProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

export interface ProductBrand {
    brandId: number;
    name: string;
    validityFrom: Date;
    validityTo: Date;
}

export interface Product {
    productId: number;
    name: string;
    description: string;
    imageUrl: string;
    productCategory: ProductCategory;
    productBrand: ProductBrand;
    price: number;
    validityFrom: Date;
    validityTo: Date;
}

@Injectable()
export class ProductSearchProvider {

  private baseApiUrl = 'http://devcarrefourstore.ns0.it:8080/carrefourstore-resteasy/services/product/';
  private baseImageApiUrl = 'http://devcarrefourstore.ns0.it:8080/image-server/services/carrefour/product/image/';

  constructor(public http: HttpClient) {
    console.log('Hello ProductSearchProvider Provider');
  }

  findProductsByCategory(categoryId, startIndex, numItemsForPage): Observable<Array<Product>> {

   /*
    const httpOptions = new HttpParams()
       .set('startIndex', startIndex)
       .set('numItemsForPage', numItemsForPage);
  */

    return this.http.get(this.baseApiUrl + "category/" + categoryId)
                    .pipe(
                      map(res => this.extractData(res, startIndex, numItemsForPage) ),
                      catchError (this.handleError)
                    );
  }

  findProductsByCategoryAndNameLike(categoryId, text, startIndex, numItemsForPage): Observable<Array<Product>> {

    //const httpOptions = new HttpParams()
       //.set('startIndex', text)
       //.set('numItemsForPage', numItemsForPage)
    //   .set('queryTextFilter', text);

    return this.http.get(this.baseApiUrl + "category/filter/" + categoryId + "?queryTextFilter=" + text)
                    .pipe(
                      map(res => this.extractData(res, startIndex, numItemsForPage) ),
                      catchError (this.handleError)
                    );
  }

  private extractData(res: any, startIndex, numItemsForPage) {
  //private extractData(res) {
    let body = res;
    console.log('response data: ' + JSON.stringify(body));
    //let userLatLon = this.userLatLon;
    let result = [] as Array<Product>;
    let validityFrom = null;
    let validityTo = null;

    if(Array.isArray(body)){
      for(var origItem of body ){

          if(origItem['validityFrom'] != undefined &&
               origItem['validityFrom'] != null){
              validityFrom = moment(origItem['validityFrom']).toDate();
          }
          else{
            validityFrom = moment().toDate();
          }

          if(origItem['validityTo'] != undefined &&
               origItem['validityTo'] != null){
              validityTo = moment(origItem['validityTo']).toDate();
          }

          let productCategory = {
            "categoryId": origItem['productCategory'].categoryId,
            "name": origItem['productCategory'].name,
            "description": origItem['productCategory'].description,
            "validityFrom" : null,
            "validityTo" : null
          } as ProductCategory;

          let productBrand = {
            "brandId": origItem['productBrand'].categoryId,
            "name": origItem['productBrand'].name,
            "validityFrom" : null,
            "validityTo" : null
          } as ProductBrand;

          let item = {
            "productId": origItem['productId'],
            "name": origItem['name'],
            "description": origItem['description'],
            "imageUrl": this.baseImageApiUrl + origItem['productId'] + "?format=png",
            "productCategory": productCategory,
            "productBrand" : productBrand,
            "price": origItem['price'],
            "validityFrom" : validityFrom,
            "validityTo" : validityTo
          } as Product;

          console.log('analyzing image url: ' + item.imageUrl);

          result.push(item);
        }
      }

      //TODO remove that pagination in-memory for server-side pagination
      if(result.length > 0){
        return result.slice(startIndex, startIndex + numItemsForPage);
      }

      return result;
    }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const err = error || '';
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
