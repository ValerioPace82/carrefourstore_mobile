import { HttpClient,HttpParams,HttpHeaders  } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from "rxjs/operators";
import { DeliveryAddress } from "../delivery-address-rest/delivery-address-rest";
import { DistributionPoint } from "../distribution-point-rest/distribution-point-rest";
import moment from'moment';

/*
  Generated class for the UserAuthenticationRestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

export interface User {
    userId: number;
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    role: string;
    oath2Bearer: string;
    subscriptionDatetime: Date;
    expirationDatetime: Date;
    deliveryAddresses: Array<DeliveryAddress>;
    defaultDistributionPoint: DistributionPoint;
}

export interface UserRegistration extends User {
   dateOfBirth: Date;
}

export interface OauthRequest {
   username: string;
   redirect_uri: string;
   grant_type: string;
   client_id: string;
   client_secret: string;
}

export interface OAuth2RefreshToken{
   value: string;
}

export interface CarrefourStoreOauth2AccessToken {
   value: string;
   expiration: Date;
   tokenType: string;
   refreshToken: OAuth2RefreshToken;
   scope: Array<string>;
   additionalInformation: any;
}

@Injectable()
export class UserAuthenticationRestProvider {

  private baseApiUrl = 'http://devcarrefourstore.ns0.it:8080/carrefourstore-resteasy/services/oauth2/gateway/';

  constructor(public http: HttpClient) {

  }

  registerUser(user: UserRegistration): Observable<User> {

    return this.http.post<User>(this.baseApiUrl + "registerUser", user, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
    })
      .pipe(
        map(res => this.extractData(res) ),
        catchError(this.handleError)
      );
  }

  findRegisteredUser(username): Observable<User> {

    return this.http.get(this.baseApiUrl + "findRegisteredUser/" + username)
                    .pipe(
                      map(res => this.extractData(res) ),
                      catchError (this.handleError)
                    );
  }

  sendLoginRequest (tokenRequest: OauthRequest): Observable<CarrefourStoreOauth2AccessToken> {

    // Add safe, URL encoded search parameter if there is a search term
  // const httpOptions = term ?
  //  { params: new HttpParams().set('name', term) } : {};
   tokenRequest.redirect_uri = this.baseApiUrl + tokenRequest.redirect_uri;
   tokenRequest.grant_type = "client_credentials";
   tokenRequest.client_id = "942564665891497";
   tokenRequest.client_secret = "f214842923fc3334c90c44538368d28d";

  /*
   let httpOptions = new URLSearchParams();
   body.set('username', tokenRequest.username);
   body.set('redirect_uri', tokenRequest.redirect_uri);
   body.set('grant_type', tokenRequest.grant_type);
   body.set('client_id', tokenRequest.client_id);
   body.set('client_secret', tokenRequest.client_secret);
   */

  const httpOptions = new HttpParams()
     .set('username', tokenRequest.username)
     .set('redirect_uri', tokenRequest.redirect_uri)
     .set('grant_type', tokenRequest.grant_type)
     .set('client_id', tokenRequest.client_id)
     .set('client_secret', tokenRequest.client_secret);

    return this.http.post<OauthRequest>(this.baseApiUrl + "login", httpOptions.toString(), {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    })
      .pipe(
        map(res => this.extractToken(res) ),
        catchError(this.handleError)
      );
  }

  private extractData(res: any) {
  //private extractData(res) {
    let body = res;
    console.log('response data: ' + JSON.stringify(body));
    //alert('response data: ' + JSON.stringify(body));
    //let userLatLon = this.userLatLon;
    let result = [] as Array<User>;
    let subscriptionDatetime = null;

    if(Array.isArray(body)){
      for(var origItem of body ){

        if(origItem['subscriptionDatetime'] != undefined &&
             origItem['subscriptionDatetime'] != null){
            subscriptionDatetime = moment(origItem['subscriptionDatetime']).toDate();
        }
        else{
          subscriptionDatetime = moment().toDate();
        }

        let item = {
          "userId": origItem['userId'],
          "username": origItem['username'],
          "firstName": origItem['firstName'],
          "lastName": origItem['lastName'],
          "email": origItem['email'],
          "role": origItem['role'],
          "oath2Bearer": origItem['oath2Bearer'],
          "subscriptionDatetime" : subscriptionDatetime,
          "expirationDatetime": null,
          "deliveryAddresses": [],
          "defaultDistributionPoint": null} as User;

        result.push(item);
      }

      return result;
    }

    if(body != null && body['username'] != null && body['username'] != ""){

      if(body['subscriptionDatetime'] != undefined &&
           body['subscriptionDatetime'] != null){
          subscriptionDatetime = moment(body['subscriptionDatetime']).toDate();
      }
      else{
        subscriptionDatetime = moment().toDate();
      }

      let item = {
        "userId": body['userId'],
        "username": body['username'],
        "firstName": body['firstName'],
        "lastName": body['lastName'],
        "email": body['email'],
        "role": body['role'],
        "oath2Bearer": body['oath2Bearer'],
        "subscriptionDatetime" : subscriptionDatetime,
        "expirationDatetime": null,
        "deliveryAddresses": [],
        "defaultDistributionPoint": null
      } as User;


        if(body['deliveryAddresses'] != undefined
           && body['deliveryAddresses'] != null
           && body['deliveryAddresses'].length > 0){

             let deliveryAddresses = body['deliveryAddresses'];

             deliveryAddresses.forEach(function(deliveryAddr){

                item.deliveryAddresses.push(deliveryAddr);
             });
        }

        if(body['defaultDistributionPoint'] != undefined
          && body['defaultDistributionPoint'] != null){

            let defaultDistributionPoint = body['defaultDistributionPoint'];

            item.defaultDistributionPoint = defaultDistributionPoint;
          }

        return item;
    }

    return {
      "userId": -1,
      "username": "",
      "firstName": "",
      "lastName": "",
      "email": "",
      "role": "",
      "oath2Bearer": "",
      "subscriptionDatetime" : null,
      "expirationDatetime": null,
      "deliveryAddresses": [],
      "defaultDistributionPoint": null} as User;

  }

  private extractToken(res: any) {
    //private extractData(res) {
      let origItem = res;

      let refreshToken = {"value": ""} as OAuth2RefreshToken;

      if(origItem['refreshToken'] != undefined && origItem['refreshToken'] != null){
        refreshToken.value = origItem['refreshToken'].value;
      }

      let item = {
        "value": origItem['access_token'],
        //"expiration": moment(origItem['expiration'],'dd/MM/yyyy'),
        "tokenType": origItem['token_type'] != undefined ? origItem['token_type'] : "",
        "refreshToken": refreshToken,
        "expiration": moment().add(origItem['expiration'],'ms').toDate(),
        "additionalInformation": origItem['additionalInformation'] != null ? origItem['additionalInformation'] : {}
        } as CarrefourStoreOauth2AccessToken;

    return item;
  //  return body.json() || { };
  }


  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const err = error || '';
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    //alert(errMsg);
    return Observable.throw(errMsg);
  }

}
