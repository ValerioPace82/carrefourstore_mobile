import { HttpClient,HttpParams,HttpHeaders  } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from "rxjs/operators";
import moment from'moment';

import { DistributionPoint } from '../distribution-point-rest/distribution-point-rest';
import { User } from '../user-authentication-rest/user-authentication-rest';
import { ProductCategory } from '../product-category/product-category';
import { Product, ProductBrand } from '../product-search/product-search';
/*
  Generated class for the OrderServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

export interface OrderInvoiceItem {
    itemId: number;
    orderInvoiceId: number;
    quantity: number;
    price: number;
    product: Product;
}

export interface OrderInvoice {
    orderInvoiceId: number;
    creationTimestamp: Date;
    confirmTimestamp: Date;
    revokeTimestamp: Date;
    distributionPoint: DistributionPoint;
    user: User;
    state: string;
    items: Array<OrderInvoiceItem>;
}

@Injectable()
export class OrderServiceProvider {

  private baseApiUrl = 'http://devcarrefourstore.ns0.it:8080/carrefourstore-resteasy/services/purchase-order/';

  constructor(public http: HttpClient) {
    
  }

  confirmOrder(orderInvoice: OrderInvoice): Observable<OrderInvoice>{

    return this.http.post<OrderInvoice>(this.baseApiUrl + 'createAndConfirm',orderInvoice, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
    })
    .pipe(
      map(res => this.extractData(res) ),
      catchError (this.handleError)
    );
  }

  revokeOrder(orderInvoice: OrderInvoice): Observable<OrderInvoice>{

    return this.http.post<OrderInvoice>(this.baseApiUrl + 'createAndConfirm',orderInvoice, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
    })
    .pipe(
      map(res => this.extractData(res) ),
      catchError (this.handleError)
    );
  }

  private extractData(res: any) {
  //private extractData(res) {
    let body = res;
    console.log('response data: ' + JSON.stringify(body));

    if(body != null){

    }

    return {
      "orderInvoiceId": -1,
      "creationTimestamp": null,
      "confirmTimestamp": null,
      "revokeTimestamp": null,
      "distributionPoint": null,
      "user": null,
      "state": "",
      "items": []
    } as OrderInvoice;

  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const err = error || '';
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    //alert(errMsg);
    return Observable.throw(errMsg);
  }

}
