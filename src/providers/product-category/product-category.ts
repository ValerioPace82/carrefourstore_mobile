import { HttpClient,HttpParams,HttpHeaders  } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from "rxjs/operators";
import moment from'moment';

/*
  Generated class for the ProductCategoryProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

export interface ProductCategory {
    categoryId: number;
    name: string;
    description: string;
    validityFrom: Date;
    validityTo: Date;
}

@Injectable()
export class ProductCategoryProvider {

  private baseApiUrl = 'http://devcarrefourstore.ns0.it:8080/carrefourstore-resteasy/services/category/';

  constructor(public http: HttpClient) {
    console.log('Hello ProductCategoryProvider Provider');
  }

  findValidProductCategories(): Observable<Array<ProductCategory>> {

    return this.http.get(this.baseApiUrl + "all")
                    .pipe(
                      map(res => this.extractData(res) ),
                      catchError (this.handleError)
                    );
  }

  private extractData(res: any) {
  //private extractData(res) {
    let body = res;
    //let userLatLon = this.userLatLon;
    let result = [] as Array<ProductCategory>;
    let validityFrom = null;
    let validityTo = null;

    if(Array.isArray(body)){
      for(var origItem of body ){

        if(origItem['validityFrom'] != undefined &&
             origItem['validityFrom'] != null){
            validityFrom = moment(origItem['validityFrom']).toDate();
        }
        else{
          validityFrom = moment().toDate();
        }

        if(origItem['validityTo'] != undefined &&
             origItem['validityTo'] != null){
            validityTo = moment(origItem['validityTo']).toDate();
        }

        let item = {
          "categoryId": origItem['categoryId'],
          "name": origItem['name'],
          "description": origItem['description'],
          "validityFrom" : validityFrom,
          "validityTo" : validityTo} as ProductCategory;

          result.push(item);
          }
        }

      return result;
    }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const err = error || '';
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
