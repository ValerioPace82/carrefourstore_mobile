import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the DeliveryAddressRestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

export interface DeliveryAddress {
    deliveryAddressId: number;
    address: string;
    state: string;
    city: string;
    region: string;
    district: string;
    cap: string;
    geoLng: number;
    geoLat: number;
    defaultAddressFlag: boolean;
    progIndex: number;
}

@Injectable()
export class DeliveryAddressRestProvider {

  constructor(public http: HttpClient) {
    console.log('Hello DeliveryAddressRestProvider Provider');
  }

}
