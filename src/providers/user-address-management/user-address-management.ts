import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from "rxjs/operators";
import { User } from "../user-authentication-rest/user-authentication-rest";
import { DeliveryAddress } from "../delivery-address-rest/delivery-address-rest";
import { DistributionPoint } from "../distribution-point-rest/distribution-point-rest";
import moment from'moment';

/*
  Generated class for the UserAddressManagementProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/


@Injectable()
export class UserAddressManagementProvider {

  private baseApiUrl = 'http://devcarrefourstore.ns0.it:8080/carrefourstore-resteasy/services/address/';

  constructor(public http: HttpClient) {
    console.log('Hello UserAddressManagementProvider Provider');
  }

  setDefaultAddress (user: User): Observable<User> {

    return this.http.post<User>(this.baseApiUrl + "setDefaultAddress", user, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
    })
      .pipe(
        map(res => this.extractData(res) ),
        catchError(this.handleError)
      );
  }

  addAddress (user: User): Observable<User> {

    return this.http.post<User>(this.baseApiUrl + "addAddress", user, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
    })
      .pipe(
        map(res => this.extractData(res) ),
        catchError(this.handleError)
      );
  }

  private extractData(res: any) {
  //private extractData(res) {
    let body = res;
    console.log('response data: ' + JSON.stringify(body));
    //let userLatLon = this.userLatLon;
    let result = [] as Array<User>;

    if(Array.isArray(body)){
      for(var origItem of body ){
        let item = {
          "userId": origItem['userId'],
          "username": origItem['username'],
          "firstName": origItem['firstName'],
          "lastName": origItem['lastName'],
          "email": origItem['email'],
          "role": origItem['role'],
          "oath2Bearer": origItem['oath2Bearer'],
          "subscriptionDatetime" : moment().toDate(),
          "expirationDatetime": null} as User;

        result.push(item);
      }

      return result;
    }

    if(body != null && body['username'] != null && body['username'] != ""){
      let item = {
        "userId": body['userId'],
        "username": body['username'],
        "firstName": body['firstName'],
        "lastName": body['lastName'],
        "email": body['email'],
        "role": body['role'],
        "oath2Bearer": body['oath2Bearer'],
        "subscriptionDatetime" : moment().toDate(),
        "expirationDatetime": null,
        "deliveryAddresses": []} as User;

        if(body['deliveryAddresses'] != undefined
           && body['deliveryAddresses'] != null
           && body['deliveryAddresses'].length > 0){

             let deliveryAddresses = body['deliveryAddresses'];

             deliveryAddresses.forEach(function(deliveryAddr){

                item.deliveryAddresses.push(deliveryAddr);
             });
        }

        return item;
    }

    return {
      "userId": -1,
      "username": "",
      "firstName": "",
      "lastName": "",
      "email": "",
      "role": "",
      "oath2Bearer": "",
      "subscriptionDatetime" : null,
      "expirationDatetime": null,
      "deliveryAddresses": []} as User;

  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const err = error || '';
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
