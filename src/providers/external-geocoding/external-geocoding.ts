import { HttpClient,HttpParams,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from "rxjs/operators";
import { LatLon } from '../distribution-point-rest/geodesic';

/*
  Generated class for the ExternalGeocodingProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
export interface ReverseGeoCoding{
  road: string,
  town: string,
  county: string,
  state: string,
  postcode: string,
  country: string
}

export interface GeoCodingLocation {
  region: string,
  province: string,
  latLng: LatLon,
  address: string,
  city: string,
  cap: string
  //adminArea3
}
export interface GeoCodingInfo {
  locations: Array<GeoCodingLocation>
}

@Injectable()
export class ExternalGeocodingProvider {

  private apiUrl = 'https://open.mapquestapi.com/geocoding/v1/address/';
  private reverseApiUrl = 'https://open.mapquestapi.com/nominatim/v1/reverse.php';


  constructor(public http: HttpClient) {
    console.log('Initialziing ExternalGeocodingProvider Provider');
  }

  findGeoCodingInfo(location): Observable<GeoCodingInfo>{

    const httpOptions = new HttpParams()
       .set('key', "T5xZMMzG6kNicj3hqWLvwqWlBHWKYGxu")
       .set('format', "json")
       .set('maxResults', "1")
       .set('location', location);

    return this.http.get(this.apiUrl, {"params": httpOptions})
                    .pipe(
                      map(res => this.extractData(res) ),
                      catchError (this.handleError)
                    );
  }

  locateGeoCodingInfo(lat, lng): Observable<ReverseGeoCoding>{

    const httpOptions = new HttpParams()
       .set('key', "T5xZMMzG6kNicj3hqWLvwqWlBHWKYGxu")
       .set('format', "json")
       .set('lat', lat)
       .set('lon', lng);

    return this.http.get(this.reverseApiUrl, {"params": httpOptions})
                    .pipe(
                      map(res => this.extractReverseData(res) ),
                      catchError (this.handleError)
                    );
  }

  private extractData(res: any) {
  //private extractData(res) {
    let body = res;
    let result = {locations: []} as GeoCodingInfo;

    if(body != null && body['info'] != null){
      let info = body['info'];

      if(info.statuscode === 0){
        for(let iresult of body.results){
          for(let location of iresult.locations){
            let geoLoc = {region: "", province: "", latLng: null, address: "", city: "", cap: "" } as GeoCodingLocation;

            geoLoc.region = location.adminArea3 != undefined && location.adminArea3 != null ? location.adminArea3 : "";
            geoLoc.province = location.adminArea4 != undefined && location.adminArea4 != null ? location.adminArea4 : "";
            geoLoc.latLng = new LatLon(location.latLng.lat, location.latLng.lng);

            result.locations.push(geoLoc);
          }
        }
      }

    }

    return result;

  }

  private extractReverseData(res: any) {

    let body = res.address;
    let result = {
      road: "",
      town: "",
      county: "",
      state: "",
      postcode: "",
      country: ""
    } as ReverseGeoCoding;

    if(body != null){
       result.road = body['road'];
       result.town = body['town'];
       if(result.town == undefined || result.town == null){
         result.town = body['city'];
       }
       result.county = body['county'];
       result.state = body['state'];
       result.postcode = body['postcode'];
       result.country = body['country'];
    }

    return result;
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const err = error || '';
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
