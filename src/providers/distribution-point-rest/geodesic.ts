import { Injectable } from '@angular/core';
export class LatLon {

  constructor(private _lat : number, private _lng: number){

  }

  get lat(){
    return this._lat;
  }

  get lng(){
    return this._lng;
  }
}

const R : number = 6378137;  // Radius of the earth in km towards Google

@Injectable()
export class GeodesicProvider {

  constructor(){

  }

  public getDistanceBetween(latLonFrom : LatLon, latLonTo : LatLon){

     return this.getDistanceFromLatLonInMeters(latLonFrom.lat, latLonFrom.lng,
        latLonTo.lat, latLonTo.lng );
  }

  private getDistanceFromLatLonInMeters(lat1,lon1,lat2,lon2) {
  //  var R = 6371000; // Radius of the earth in meters
    var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2-lon1);
    var a =
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon/2) * Math.sin(dLon/2)
      ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c; // Distance in km
    return d;
  }

  private deg2rad(deg) {
    return deg * (Math.PI/180)
  }
}
