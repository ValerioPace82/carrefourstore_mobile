import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from "rxjs/operators";
import { GeodesicProvider, LatLon } from './geodesic';
import { User } from "../user-authentication-rest/user-authentication-rest";
import moment from'moment';

/*
  Generated class for the DistributionPointRestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

export interface DistributionPointExt {
    distributionPointId: number;
    name: string;
    address: string;
    distanceFromUser: number;
    geoLat: number;
    geoLng: number;
}

export interface DistributionPoint {
    distributionPointId: number;
    name: string;
    address: string;
    geoLat: number;
    geoLng: number;
}

@Injectable()
export class DistributionPointRestProvider {

  private baseApiUrl = 'http://devcarrefourstore.ns0.it:8080/carrefourstore-resteasy/services/distributionPoint/';
  private userBaseApiUrl = 'http://devcarrefourstore.ns0.it:8080/carrefourstore-resteasy/services/address/';


  //how can I pass this parameter as a lambda function args instead?????  It can be passed directly in outer method arguments, beware of its type
  //userLatLon: LatLon;

  constructor(public http: HttpClient, public geodesicProvider: GeodesicProvider) {

  }

  findNearestDistributionPointsByCap(cap): Observable<Array<DistributionPoint>> {

    return this.http.get(this.baseApiUrl + "findByCap/" + cap)
                    .pipe(
                      map(res => this.extractDistributionPointData(res) ),
                      catchError (this.handleError)
                    );
  }

  findNearestDistributionPointsExtByCap(cap, userLatLon): Observable<Array<DistributionPointExt>> {

    return this.http.get(this.baseApiUrl + "findByCap/" + cap)
                    .pipe(
                      map(res => this.extractDistributionPointExtData(res, userLatLon) ),
                      catchError (this.handleError)
                    );
  }

  setDefaultDistributionPoint(user: User): Observable<User> {

    //this.userLatLon = userLatLon;

    return this.http.post<User>(this.userBaseApiUrl + "setDefaultDistributionPoint", user, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
    })
      .pipe(
        map(res => this.extractDataUser(res) ),
        catchError(this.handleError)
      );
  }

  private extractDistributionPointExtData(res: any, userLatLon: LatLon) {
  //private extractData(res) {
    let body = res;
    //let userLatLon = this.userLatLon;
    let result = [] as Array<DistributionPointExt>;

    if(Array.isArray(body)){
      for(var origItem of body ){
        let item = {
         "distributionPointId": origItem['distributionPointId'],
         "name": origItem['name'],
         "address": origItem['address'],
          "distanceFromUser" : 0.0,
          "geoLat": origItem['geoLat'],
          "geoLng": origItem['geoLng']} as DistributionPointExt;
        let dpLatLon = new LatLon(origItem['geoLat'], origItem['geoLng']);
        item.distanceFromUser = this.geodesicProvider.getDistanceBetween(userLatLon, dpLatLon);

        result.push(item);
      }

      return result;
    }

    let item = {
     "distributionPointId": body['distributionPointId'],
     "name": body['name'],
     "address": body['address'],
      "distanceFromUser" : 0.0,
      "geoLat": body['geoLat'],
      "geoLng": body['geoLng']
    } as DistributionPointExt;

    let dpLatLon = new LatLon(body['geoLat'], body['geoLng']);
    item.distanceFromUser = this.geodesicProvider.getDistanceBetween(userLatLon, dpLatLon);

    return item;
  //  return body.json() || { };
  }

  private extractDistributionPointData(res: any) {
  //private extractData(res) {
    let body = res;
    //let userLatLon = this.userLatLon;
    let result = [] as Array<DistributionPoint>;

    if(Array.isArray(body)){
      for(var origItem of body ){
        let item = {
         "distributionPointId": origItem['distributionPointId'],
         "name": origItem['name'],
         "address": origItem['address'],
          "geoLat": origItem['geoLat'],
          "geoLng": origItem['geoLng']} as DistributionPoint;

        result.push(item);
      }

      return result;
    }

    let item = {
     "distributionPointId": body['distributionPointId'],
     "name": body['name'],
     "address": body['address'],
      "geoLat": body['geoLat'],
      "geoLng": body['geoLng']
    } as DistributionPoint;

    return item;
  //  return body.json() || { };
  }

  private extractDataUser(res: any) {
    //private extractData(res) {
      let body = res;

      if(body != null && body['username'] != null && body['username'] != ""){
        let item = {
          "userId": body['userId'],
          "username": body['username'],
          "firstName": body['firstName'],
          "lastName": body['lastName'],
          "email": body['email'],
          "role": body['role'],
          "oath2Bearer": body['oath2Bearer'],
          "subscriptionDatetime" : moment().toDate(),
          "expirationDatetime": null,
          "deliveryAddresses": []} as User;

          if(body['deliveryAddresses'] != undefined
             && body['deliveryAddresses'] != null
             && body['deliveryAddresses'].length > 0){

               let deliveryAddresses = body['deliveryAddresses'];

               deliveryAddresses.forEach(function(deliveryAddr){

                  item.deliveryAddresses.push(deliveryAddr);
               });
          }

          return item;
      }

      return {
        "userId": -1,
        "username": "",
        "firstName": "",
        "lastName": "",
        "email": "",
        "role": "",
        "oath2Bearer": "",
        "subscriptionDatetime" : null,
        "expirationDatetime": null,
        "deliveryAddresses": []} as User;
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const err = error || '';
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }


}
