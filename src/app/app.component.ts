import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav } from 'ionic-angular';

import { TabsPage } from '../pages/tabs/tabs';
import { PurchasePage } from '../pages/purchase/purchase';
import { UserProfilePage } from '../pages/userProfile/userProfile';
import { CarrefourMapPage } from '../pages/carrefourMap/carrefourMap';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { User } from '../providers/user-authentication-rest/user-authentication-rest';

export interface MenuItem {
    title: string;
    component: any;
    icon: string;
    tabIndex: number;
}

@Component({
  templateUrl: 'app.html'
})
export class CarrefourStoreMobileApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = TabsPage;

  menuItems: Array<MenuItem>;
  user: User;

  constructor(public platform: Platform,
    public menu: MenuController,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public events: Events,
    private storage: Storage) {
    this.initializeApp();

    this.user = null;

    this.menuItems = [
        {title: 'Carrefour Map', component: CarrefourMapPage, icon: 'map', tabIndex: 0},
        {title: 'Fai la tua spesa', component: PurchasePage, icon: 'cart', tabIndex: 1},
        {title: 'Profilo Utente', component: UserProfilePage, icon: 'man', tabIndex: 2}
    ];

    events.subscribe('user:created', (user, time) => {
       // user and time are the same arguments passed in `events.publish(user, time)`
       console.log('Welcome', user, 'at', time);
       this.storage.set('user', user);

       this.user = user;

       this.openPage(this.menuItems[2]);
    });

    events.subscribe('user:authenticate', (user, time) => {
       // user and time are the same arguments passed in `events.publish(user, time)`
       console.log('Welcome', user, 'at', time);
       this.storage.set('user', user);
       this.user = user;

       this.openPage(this.menuItems[0]);
       //this.nav.setRoot(this.rootPage, {"tabIndex": 0}, {animate: true, direction: 'back'});
    });

    events.subscribe('user:logout', (user, time) => {
       // user and time are the same arguments passed in `events.publish(user, time)`
       console.log('Goodbye', user, 'at', time);
       this.storage.clear();
       this.user = null;

       this.openPageNoSelection(this.menuItems[2]);

       // maybe animation transition make troubles with Leaflet/angular component???
       //this.nav.setRoot(this.rootPage, {"tabIndex": 2}, {animate: true, direction: 'back'});
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(this.rootPage, {"tabIndex": page.tabIndex, "changeSelection": true});

  }

  openPageNoSelection(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(this.rootPage, {"tabIndex": page.tabIndex, "changeSelection": false});

  }

  logout() {
    // close the menu when clicking a link from the menu
    this.menu.close();

    this.events.publish('user:logout', this.user, Date.now());
  }

}
