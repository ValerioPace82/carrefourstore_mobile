import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { TooltipsModule } from 'ionic-tooltips';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { CarrefourStoreMobileApp } from './app.component';

import { PurchasePage } from '../pages/purchase/purchase';
import { UserProfilePage } from '../pages/userProfile/userProfile';
import { CarrefourMapPage } from '../pages/carrefourMap/carrefourMap';
import { TabsPage } from '../pages/tabs/tabs';
import { NearestDistributionPointsPage } from '../pages/nearest-distribution-points/nearest-distribution-points';
import { AddDeliveryAddressPage } from '../pages/add-delivery-address/add-delivery-address';
import { ProductListPage } from '../pages/product-list/product-list';
import { PurchaseCartPage } from '../pages/purchase-cart/purchase-cart';
import { UserRegistrationPage } from '../pages/user-registration/user-registration';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Geolocation } from '@ionic-native/geolocation';
import { DistributionPointRestProvider } from '../providers/distribution-point-rest/distribution-point-rest';
import { GeodesicProvider } from '../providers/distribution-point-rest/geodesic';

import { IonicStorageModule } from '@ionic/storage';
import { UserAuthenticationRestProvider } from '../providers/user-authentication-rest/user-authentication-rest';
import { DeliveryAddressRestProvider } from '../providers/delivery-address-rest/delivery-address-rest';
import { ExternalGeocodingProvider } from '../providers/external-geocoding/external-geocoding';
import { UserAddressManagementProvider } from '../providers/user-address-management/user-address-management';
import { ProductCategoryProvider } from '../providers/product-category/product-category';
import { ProductSearchProvider } from '../providers/product-search/product-search';
import { OrderServiceProvider } from '../providers/order-service/order-service';

import { AddProductPopoverPageModule } from '../pages/add-product-popover/add-product-popover.module';
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
  declarations: [
    CarrefourStoreMobileApp,
    PurchasePage,
    UserProfilePage,
    CarrefourMapPage,
    NearestDistributionPointsPage,
    ProductListPage,
    PurchaseCartPage,
    AddDeliveryAddressPage,
    UserRegistrationPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(CarrefourStoreMobileApp),
    TooltipsModule,
    IonicStorageModule.forRoot({
      name: '__mydb',
         driverOrder: ['sqlite','indexeddb','websql']
    }),
    HttpClientModule,
    AddProductPopoverPageModule,
    PipesModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    CarrefourStoreMobileApp,
    PurchasePage,
    ProductListPage,
    PurchaseCartPage,
    UserProfilePage,
    NearestDistributionPointsPage,
    AddDeliveryAddressPage,
    UserRegistrationPage,
    CarrefourMapPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DistributionPointRestProvider,
    GeodesicProvider,
    UserAuthenticationRestProvider,
    DeliveryAddressRestProvider,
    ExternalGeocodingProvider,
    UserAddressManagementProvider,
    ProductCategoryProvider,
    ProductSearchProvider,
    OrderServiceProvider
  ]
})
export class AppModule {}
