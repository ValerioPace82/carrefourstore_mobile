import { Component, ViewChild } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from "@angular/animations";
import { NavController, ToastController  } from 'ionic-angular';
import { AddDeliveryAddressPage } from '../add-delivery-address/add-delivery-address';
import { UserRegistrationPage } from '../user-registration/user-registration';
import { UserAddressManagementProvider } from "../../providers/user-address-management/user-address-management";
import { DeliveryAddress } from "../../providers/delivery-address-rest/delivery-address-rest";
import { User, UserAuthenticationRestProvider, CarrefourStoreOauth2AccessToken, OauthRequest} from '../../providers/user-authentication-rest/user-authentication-rest';
import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import moment from'moment';
import * as _ from 'underscore';

@Component({
  selector: 'page-userProfile',
  templateUrl: 'userProfile.html',
  animations: [
        trigger('itemState', [
            transition('void => *', [
                style({transform: 'translateX(-100%)', opacity: 1}),
                animate('600ms ease-out')
            ]),
            transition('* => void', [
                animate('600ms ease-in', style({transform: 'translateX(100%)', opacity: 0}))
            ])
        ])
    ]
})
export class UserProfilePage {

  @ViewChild('email') email: any;
  loginView: boolean;
  errorMessage: string;
  user: User;
  accessToken: CarrefourStoreOauth2AccessToken;
  addDeliveryAddressPage:any = AddDeliveryAddressPage;
  userRegistrationPage:any = UserRegistrationPage;

  constructor(public navCtrl: NavController,
  public userAuthenticationRest: UserAuthenticationRestProvider,
  public userAddressManagementRest: UserAddressManagementProvider,
  private toastCtrl: ToastController,
  public events: Events,
  private storage: Storage) {

     this.user = {"userId":-1,
              "username": "",
              "firstName" : "",
              "lastName" : "",
              "email": "",
              "role": "",
              "oath2Bearer": "",
              "subscriptionDatetime": null,
              "expirationDatetime": null} as User;
     this.errorMessage="";
  }

  ionViewWillEnter() {
    console.log('ionViewDidLoad UserProfilePage');

    this.storage.get('user').then((user) => {

        if(user != undefined
            && user != null && user.oath2Bearer != ""){
          console.log('JSON user in storage', JSON.stringify(user));
          this.loginView = false;
          this.user = user;
          this.initializeUserProfileView(this.user);
        }
        else{
          this.loginView = true;
          this.initializeLoginView();
        }
    });
  }

  initializeLoginView() {

    console.log('initialize Login view');

    setTimeout(() => {
      this.email.setFocus();
    }, 500);
  }

  initializeUserProfileView(user: User ){

  }

  findRegisteredUser(username){
    this.userAuthenticationRest.findRegisteredUser(username)
     .subscribe(
       userResult => this.user = userResult,
       error =>  this.errorMessage = <any>error);
  }

  sendLoginRequest(tokenRequest: OauthRequest){

    this.userAuthenticationRest.sendLoginRequest(tokenRequest)
     .subscribe(
       accessToken => this.accessToken = accessToken,
       error =>  this.errorMessage = <any>error);
  }

  login(){
    console.log('trying to login user: ' + this.user.username);
    let usernameLogin = this.user.username;
    this.findRegisteredUser(this.user.username);

    setTimeout(() => {
      this.email.setFocus();

      if(this.user.userId != -1){
        console.log('user found: ' + JSON.stringify(this.user));
        this.errorMessage = "";

        let tokenRequest = {
          "username": this.user.username,
          "redirect_uri": "/login",
          "grant_type": "",
          "client_id": "",
          "client_secret": ""
        } as OauthRequest;

        this.sendLoginRequest(tokenRequest);

        setTimeout(() => {

          if(this.errorMessage.length == 0){
            console.log("access token obtained: " + JSON.stringify(this.accessToken));
          }

          if(this.errorMessage.length == 0
            && this.accessToken != null && moment(this.accessToken.expiration).toDate() < new Date()){

               this.user.oath2Bearer = this.accessToken.value;
               console.log("access token updated with user: " + JSON.stringify(this.user));

               this.events.publish('user:authenticate', this.user, Date.now());
          }

        }, 2000);


      }
      else{
        this.user.username = usernameLogin;
        console.log('authentication failure, user not found');
        this.errorMessage = 'authentication failure, user not found';
      }
    }, 2000);



  }

  updateFlag(deliveryAddr: DeliveryAddress){

    console.log('updating delivery address: ' + JSON.stringify(deliveryAddr));
    this.user.deliveryAddresses = _.map(this.user.deliveryAddresses, function(deliveryAddrObj){
      if(deliveryAddrObj.deliveryAddressId == this.deliveryAddressId){
        deliveryAddrObj.defaultAddressFlag = this.defaultAddressFlag;
      }

      return deliveryAddrObj;
    }, {"deliveryAddressId": deliveryAddr.deliveryAddressId,
        "defaultAddressFlag": deliveryAddr.defaultAddressFlag});

    console.log('updated delivery address: ' + JSON.stringify(deliveryAddr));

    this.userAddressManagementRest.setDefaultAddress(this.user)
    .subscribe(
      user => this.user,
      error =>  this.errorMessage = <any>error,
      () => this.onUpdateUserAddress()
    );
  }

  private onUpdateUserAddress(){
    let toast = this.toastCtrl.create({
      message: 'Indirizzo preferito aggiornato',
      duration: 2000,
      position: 'bottom'
    });

    toast.present();
  }

  logout(event){

    //this.navCtrl.push(this.addDeliveryAddressPage, {"user":this.user}, {animate: true, direction: 'back', duration:1500, easing:'ease-in'});
    this.events.publish('user:logout',this.user, Date.now());
  }

  addDeliveryAddress(event){

    //this.navCtrl.push(this.addDeliveryAddressPage, {"user":this.user}, {animate: true, direction: 'back', duration:1500, easing:'ease-in'});
    this.navCtrl.push(this.addDeliveryAddressPage, {"user":this.user});
  }

  registerUser(event){

    //this.navCtrl.push(this.addDeliveryAddressPage, {"user":this.user}, {animate: true, direction: 'back', duration:1500, easing:'ease-in'});
    this.navCtrl.push(this.userRegistrationPage);
  }

}
