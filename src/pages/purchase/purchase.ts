import { Component } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from "@angular/animations";
import { ToastController, NavController } from 'ionic-angular';
import { ProductListPage } from '../product-list/product-list';
import { PurchaseCartPage } from '../purchase-cart/purchase-cart';

import { User } from '../../providers/user-authentication-rest/user-authentication-rest';
import { DistributionPoint } from '../../providers/distribution-point-rest/distribution-point-rest';
import { ProductCategory, ProductCategoryProvider } from '../../providers/product-category/product-category';
import { OrderInvoice, OrderInvoiceItem, OrderServiceProvider } from '../../providers/order-service/order-service';


import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-purchase',
  templateUrl: 'purchase.html',
  animations: [
        trigger('itemState', [
            transition('void => *', [
                style({transform: 'translateX(-100%)', opacity: 1}),
                animate('600ms ease-out')
            ]),
            transition('* => void', [
                animate('600ms ease-in', style({transform: 'translateX(100%)', opacity: 0}))
            ])
        ])
    ]
})
export class PurchasePage {

  itemsCount: number;
  errorMessage: string;
  user: User;

  productCategories: Array<ProductCategory>;

  productListPage:any = ProductListPage;
  purchaseCartPage:any = PurchaseCartPage;
  currentOrder: OrderInvoice;

  constructor(public navCtrl: NavController,
    public events: Events,
    public productCategoryRest: ProductCategoryProvider,
    public orderServiceRest: OrderServiceProvider,
    private toastCtrl: ToastController,
    private storage: Storage) {
    this.itemsCount = 0;
    this.errorMessage="";
  }

  ionViewDidEnter() {

    if(this.user == null || this.user == undefined){
      this.storage.get('user').then((user) => {
        if(user != undefined
            && user != null && user.oath2Bearer != ""){

          this.user = user;
          this.loadProductCategories();
          this.itemsCount = this.reloadCart();
        }
        else{

          let toast = this.toastCtrl.create({
            message: 'Utente non autenticato, redirezione alla pagina di login',
            duration: 2000,
            position: 'top',
          });

          toast.onDidDismiss(() => {
              console.log('Dismissed toast utente non autenticato');

              this.events.publish('user:requestLogin');
          });

          toast.present();

        }
      });
    }
    else{
      this.loadProductCategories();
      this.itemsCount = this.reloadCart();
    }
  }

  resetItemsCount(){
    this.itemsCount = 0;
  }

  reloadCart(){
      this.storage.get('currentOrder').then((currentOrder) => {
        if(currentOrder != undefined && currentOrder != null){

          this.currentOrder = currentOrder;
          console.log('Current order fetched from local storage: ' + JSON.stringify(this.currentOrder));
        }
        else{

         let currentOrder = {
           "orderInvoiceId": -1,
           "creationTimestamp": new Date(),
           "confirmTimestamp": null,
           "revokeTimestamp": null,
           "distributionPoint": this.user.defaultDistributionPoint,
           "user": this.user,
           "state": "",
           "items": []
         } as OrderInvoice;
         this.currentOrder = currentOrder;
         this.storage.set('currentOrder', this.currentOrder);
         console.log('Current order created into local storage: ' + JSON.stringify(this.currentOrder));
        }

        this.itemsCount = this.currentOrder.items.length;
      });


    return this.itemsCount;
  }

  findValidProductCategories() {
    this.productCategoryRest.findValidProductCategories()
       .subscribe(
         productCategories => this.productCategories = productCategories,
         error =>  this.errorMessage = <any>error,
         () => this.onloadProductCategories() );
  }

  loadProductCategories(){

    this.findValidProductCategories();

  }

  onloadProductCategories(){
    console.log("product categories loaded: " + JSON.stringify(this.productCategories));
  }


  showProductList(category: ProductCategory){
    console.log('Showing product list page by category : ' + category.name);
    this.navCtrl.push(this.productListPage, {"category": category, "user": this.user});
  }

  showCart(){
    console.log("show Cart page");
    this.navCtrl.push(this.purchaseCartPage, {"currentOrder": this.currentOrder, "user": this.user});
  }

}
