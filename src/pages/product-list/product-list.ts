import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController  } from 'ionic-angular';

import { User } from "../../providers/user-authentication-rest/user-authentication-rest";
import { ProductCategory } from '../../providers/product-category/product-category';
import { Product, ProductSearchProvider } from '../../providers/product-search/product-search';

import { DistributionPoint } from '../../providers/distribution-point-rest/distribution-point-rest';
import { OrderInvoice, OrderInvoiceItem, OrderServiceProvider } from '../../providers/order-service/order-service';
import { Storage } from '@ionic/storage';

import { PurchaseCartPage } from '../purchase-cart/purchase-cart';
import { AddProductPopoverPage } from '../add-product-popover/add-product-popover';

import * as _ from 'underscore';
/**
 * Generated class for the ProductListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product-list',
  templateUrl: 'product-list.html',
})
export class ProductListPage {

  productCategory: ProductCategory;
  productList: Array<Product>;
  purchaseCartPage:any = PurchaseCartPage;
  queryText:  string;
  errorMessage: string;
  user: User;
  currIndex = 0;
  itemsPerPage = 5;
  itemsCount = 0;
  currentOrder: OrderInvoice;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public productSearchRest: ProductSearchProvider,
              public popoverCtrl: PopoverController,
              private storage: Storage) {

      this.productCategory = navParams.get("category");
      this.user = navParams.get("user");
      this.queryText = '';
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad ProductListPage');
    this.currIndex = 0;
    this.itemsPerPage = 5;
    this.productList = [] as Array<Product>;
    this.loadProducts();
    this.itemsCount = this.reloadCart();

  }

  reloadCart(){

      this.storage.get('currentOrder').then((currentOrder) => {
        if(currentOrder != undefined && currentOrder != null){

          this.currentOrder = currentOrder;
          console.log('Current order fetched from local storage: ' + JSON.stringify(this.currentOrder));
        }
        else{

         let currentOrder = {
           "orderInvoiceId": -1,
           "creationTimestamp": new Date(),
           "confirmTimestamp": null,
           "revokeTimestamp": null,
           "distributionPoint": this.user.defaultDistributionPoint,
           "user": this.user,
           "state": "",
           "items": []
         } as OrderInvoice;
         this.currentOrder = currentOrder;
         this.storage.set('currentOrder', this.currentOrder);
         console.log('Current order created into local storage: ' + JSON.stringify(this.currentOrder));
        }

        this.itemsCount = this.currentOrder.items.length;
      });

    return this.itemsCount;
  }

  showCart(){
    console.log("show Cart page");
    this.navCtrl.push(this.purchaseCartPage, {"currentOrder": this.currentOrder, "user": this.user});
  }

  //handler for simple submit event
  searchProductsByFilter(){
    this.onChangeQueryText(this.queryText);
  }

  onChangeQueryText(queryText){
    this.queryText = queryText;
    this.currIndex = 0;
    this.productList = [] as Array<Product>;
    this.loadProducts();
  }

  addProductToCart(product: Product){

    let popover = this.popoverCtrl.create(AddProductPopoverPage, {"product": product, "currentOrder": this.currentOrder}, {"showBackdrop": true});
    popover.present();
    popover.onDidDismiss(() => {
        this.itemsCount = this.reloadCart();
    });
  }

  loadProducts(){

    if(this.queryText != ""){
    this.productSearchRest.findProductsByCategoryAndNameLike(this.productCategory.categoryId, this.queryText, this.currIndex, this.itemsPerPage)
     .subscribe(
       res => {
         if(!_.isEmpty(res)){
           this.productList = _.union(this.productList, res);
           this.currIndex += res.length;
         }
       },
       error =>  this.errorMessage = <any>error,
       () => this.onCompleteProductsLoad() );
     }
     else{

       this.productSearchRest.findProductsByCategory(this.productCategory.categoryId, this.currIndex, this.itemsPerPage)
        .subscribe(
          res => {
            if(!_.isEmpty(res)){
              this.productList = _.union(this.productList, res);
              this.currIndex += res.length;
            }
          },
          error =>  this.errorMessage = <any>error,
          () => this.onCompleteProductsLoad() );
      }

  }

  onCompleteProductsLoad(){
    console.log("Products: " + JSON.stringify(this.productList));
  }

  /* infinite scroll pagination logic*/
  doInfinite(): Promise<any> {
    console.log('Begin async operation');

    return new Promise((resolve) => {
      setTimeout(() => {

        this.loadProducts();

        console.log('Async operation has ended');
        resolve();
      }, 500);
    })
  }


}
