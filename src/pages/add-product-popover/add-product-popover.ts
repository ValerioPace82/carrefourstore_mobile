import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { User } from "../../providers/user-authentication-rest/user-authentication-rest";
import { ProductCategory } from '../../providers/product-category/product-category';
import { Product, ProductSearchProvider } from '../../providers/product-search/product-search';

import { DistributionPoint } from '../../providers/distribution-point-rest/distribution-point-rest';
import { OrderInvoice, OrderInvoiceItem, OrderServiceProvider } from '../../providers/order-service/order-service';
import { Storage } from '@ionic/storage';
import * as _ from 'underscore';

/**
 * Generated class for the AddProductPopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-product-popover',
  templateUrl: 'add-product-popover.html',
})
export class AddProductPopoverPage {

  currentOrder: OrderInvoice;
  product: Product;
  quantity: number = 0;
  orderItemPrice: number = 0.0;

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage) {

      this.product = navParams.get("product");
      this.currentOrder = navParams.get("currentOrder");
  }

  ionViewDidLoad() {
    console.log('adding product: ' + JSON.stringify(this.product));
    console.log('current order in cart: ' + JSON.stringify(this.currentOrder));
  }

  onChangeQuantity($event){

    console.log('event onchange quantity fired');
    this.orderItemPrice = this.product.price * this.quantity;

    let orderInvoiceItem = {
      "itemId": null,
      "orderInvoiceId": this.currentOrder.orderInvoiceId,
      "quantity": this.quantity,
      "price": this.orderItemPrice,
      "product": this.product,
    } as OrderInvoiceItem;

    let itemInCart = _.findWhere(this.currentOrder.items, {"product": this.product});

    if(this.quantity > 0 && itemInCart != undefined){

       let qnty = this.quantity;
       let oitemPrice = this.orderItemPrice;

       this.currentOrder.items = _.map(this.currentOrder.items, function(item){

         if(item.product.productId == itemInCart.product.productId){
            item.quantity = qnty;
            item.price = oitemPrice;
         }

         return item;
       });
    }
    else if(this.quantity > 0){
      this.currentOrder.items.push(orderInvoiceItem);
    }
    else{
      this.currentOrder.items = _.without(this.currentOrder.items, itemInCart);
    }
  }

  addItemQuantity(){
    console.log('call increase item quantity');
    this.quantity = this.quantity + 1;

  }

  removeItemQuantity(){
    console.log('call decrease item quantity');
    if(this.quantity > 0){
      this.quantity = this.quantity - 1;
    }
  }

  ionViewWillLeave(){
    console.log('popover will be dismiss saving current order state');

    this.storage.set('currentOrder', this.currentOrder);

  }

}
