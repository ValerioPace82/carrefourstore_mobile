import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddProductPopoverPage } from './add-product-popover';

@NgModule({
  declarations: [
    AddProductPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(AddProductPopoverPage),
  ],
  exports: [
    AddProductPopoverPage
  ]
})
export class AddProductPopoverPageModule {}
