import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { User } from '../../providers/user-authentication-rest/user-authentication-rest';
import { DistributionPoint } from '../../providers/distribution-point-rest/distribution-point-rest';
import { DeliveryAddress } from "../../providers/delivery-address-rest/delivery-address-rest";
import { OrderInvoice, OrderInvoiceItem, OrderServiceProvider } from '../../providers/order-service/order-service';

import { Storage } from '@ionic/storage';
import * as _ from 'underscore';
/**
 * Generated class for the PurchaseCartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-purchase-cart',
  templateUrl: 'purchase-cart.html',
})
export class PurchaseCartPage {

  currentOrder: OrderInvoice;
  user: User;
  errorMessage: string;
  activeDeliveryAddress: DeliveryAddress;

  //only for demo purpose
  paymentCheckStatus: string = "NONE";

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public orderServiceRest: OrderServiceProvider,
              private toastCtrl: ToastController,
              private storage: Storage) {

        this.currentOrder = navParams.get('currentOrder');
        this.user = navParams.get('user');
  }

  ionViewWillLoad() {
    console.log('Loading Cart..');

    this.activeDeliveryAddress = _.findWhere(this.user.deliveryAddresses, {defaultAddressFlag: true});
    console.log('user default address: ' + this.activeDeliveryAddress.address);
  }

  ionViewWillLeave() {
    console.log('Loading Cart..');

    if(this.currentOrder != null){
      this.storage.set('currentOrder', this.currentOrder);
    }
  }

  addItemQuantity(item){
    this.currentOrder.items = _.map(this.currentOrder.items, function(currentItem){

      if(currentItem.product.productId == item.product.productId){
         currentItem.quantity = currentItem.quantity + 1;
         currentItem.price = currentItem.product.price * currentItem.quantity;
      }

      return currentItem;
    });
  }

  removeItemQuantity(item){

    let itemInCart = _.findWhere(this.currentOrder.items, {"product": item.product});

    if(itemInCart.quantity == 1){
      this.removeItem(item);
    }
    else{

      this.currentOrder.items = _.map(this.currentOrder.items, function(currentItem){

        if(currentItem.product.productId == item.product.productId){
           currentItem.quantity = currentItem.quantity - 1;
           currentItem.price = currentItem.product.price * currentItem.quantity;
        }

        return currentItem;
      });

    }

  }

  removeItem(item){
    this.currentOrder.items = _.without(this.currentOrder.items, item);
  }

  fetchOrder(currentOrder){

    //this function, that access directly local storage will be ignored when back-end issues will be solved
    return this.currentOrder;

  }

  confirmOrder(){

    this.currentOrder.confirmTimestamp = new Date();
    this.orderServiceRest.confirmOrder(this.currentOrder)
           .subscribe(currentOrder => this.currentOrder = this.fetchOrder(currentOrder),
                      error => this.errorMessage = <any>error,
                      () => {
                          this.onOrderConfirmed();
                          console.log("Order confirmed: " + JSON.stringify(this.currentOrder));
                          });
  }

  revokeOrder(){

    this.currentOrder.revokeTimestamp = new Date();
    this.orderServiceRest.revokeOrder(this.currentOrder)
           .subscribe(currentOrder => this.currentOrder = this.fetchOrder(currentOrder),
                      error => this.errorMessage = <any>error,
                      () => {
                          this.onOrderRevoked();
                          console.log("Order revoked: " + JSON.stringify(this.currentOrder));
                          });
  }

  onOrderConfirmed(){
    let toast = this.toastCtrl.create({
      message: 'Ordine confermato',
      duration: 2000,
      position: 'up'
    });

    toast.present();
    this.paymentCheckStatus = "IN PROGESS";

    console.log('paymentCheckStatus: ' + this.paymentCheckStatus);
  }

  onOrderRevoked(){
    let toast = this.toastCtrl.create({
      message: 'Ordine revocato correttamente',
      duration: 2000,
      position: 'up'
    });

    toast.present();
    this.paymentCheckStatus = "NONE";
  }

  paymentCheck(){
    //demo.. do payment immediately
    this.onPaymentCheckSuccess();
  }

  onPaymentCheckSuccess(){

    let toast = this.toastCtrl.create({
      message: 'Pagamento confermato, grazie per aver scelto Carrefour Store Mobile',
      duration: 2000,
      position: 'up'
    });

    toast.present();
    this.paymentCheckStatus = "SUCCESS";

    //unload Cart after payment
    this.currentOrder = null;
    this.storage.set('currentOrder', this.currentOrder);

  }

}
