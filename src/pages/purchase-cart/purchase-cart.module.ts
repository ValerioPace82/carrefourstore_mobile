import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PurchaseCartPage } from './purchase-cart';

@NgModule({
  declarations: [
    PurchaseCartPage,
  ],
  imports: [
    IonicPageModule.forChild(PurchaseCartPage),
  ],
})
export class PurchaseCartPageModule {}
