import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Tabs } from 'ionic-angular';
import { PurchasePage } from '../purchase/purchase';
import { UserProfilePage } from '../userProfile/userProfile';
import { CarrefourMapPage } from '../carrefourMap/carrefourMap';
import { Events } from 'ionic-angular';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  @ViewChild('tabs') tabRef: Tabs;

  tab1Root = CarrefourMapPage;
  tab2Root = PurchasePage;
  tab3Root = UserProfilePage;

  constructor(public navCtrl: NavController,
     private navParams: NavParams,
     public events: Events
   ) {
    events.subscribe('user:requestLogin', () => {

       //redirect to login page
       this.selectPage(2);
    });
  }

  ionViewDidEnter() {

    let tabIndex = this.navParams.get('tabIndex');
    let changeSelection = this.navParams.get('changeSelection');

    if(changeSelection){
      this.selectPage(tabIndex);
    }
    //this parent reference for view component inside nav controller doesn't work here
    //this.navCtrl.parent.select(1);
  }

  public selectPage(tabIndex){
    //TODO handle GMap bug initialization after switching tab first and then menu click
    console.log("tab selection started");
    if(this.tabRef.getSelected() != null && this.tabRef.getSelected() != undefined
    && this.tabRef.getSelected().id != this.tabRef.getByIndex(tabIndex).id){
      console.log("tab selection for tabIndex: " + tabIndex);
      this.tabRef.select(tabIndex);
    }
    else if(this.tabRef._tabs[tabIndex]._views != null){
      console.log("tab selection for tabIndex (no sel): " + tabIndex);
      this.tabRef.select(tabIndex);
    }

    console.log("tab selection completed");
  }



}
