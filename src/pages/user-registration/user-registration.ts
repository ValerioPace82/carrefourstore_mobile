import { Component } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from "@angular/animations";
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { IonicPage, NavController, ToastController, NavParams } from 'ionic-angular';
import { User, UserRegistration, UserAuthenticationRestProvider, CarrefourStoreOauth2AccessToken, OauthRequest} from '../../providers/user-authentication-rest/user-authentication-rest';

import { Events } from 'ionic-angular';
import moment from'moment';
/**
 * Generated class for the UserRegistrationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-registration',
  templateUrl: 'user-registration.html',
  animations: [
        trigger('itemState', [
            transition('void => *', [
                style({transform: 'translateX(-100%)', opacity: 1}),
                animate('600ms ease-out')
            ]),
            transition('* => void', [
                animate('600ms ease-in', style({transform: 'translateX(100%)', opacity: 0}))
            ])
        ])
    ]
})
export class UserRegistrationPage {

  registrationForm:FormGroup;
  user: UserRegistration;
  errorMessage: string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private formBuilder: FormBuilder,
              public userAuthenticationRest: UserAuthenticationRestProvider,
              private toastCtrl: ToastController,
              public events: Events) {

        this.user = {"userId":-1,
                     "username": "",
                     "firstName" : "",
                     "lastName" : "",
                     "email": "",
                     "dateOfBirth": null,
                     "role": "",
                     "oath2Bearer": "",
                     "subscriptionDatetime": null,
                     "expirationDatetime": null} as UserRegistration;
        this.errorMessage="";
        this.registrationForm = this.formBuilder.group({
          name: ['', Validators.required],
          surname: ['', Validators.required],
          email: ['', Validators.required],
          dateOfBirth: ['', Validators.required],
        });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserRegistrationPage');
  }

  registerUser(){

    this.user = {"userId":-1,
                 "username": "",
                 "firstName" : this.registrationForm.value.name,
                 "lastName" : this.registrationForm.value.surname,
                 "email": this.registrationForm.value.email,
                 "dateOfBirth": moment(this.registrationForm.value.dateOfBirth).toDate(),
                 "role": "USER",
                 "oath2Bearer": "",
                 "subscriptionDatetime": null,
                 "expirationDatetime": null} as UserRegistration;

    this.user.username = this.user.email;
    console.log('User registration for: ' + JSON.stringify(this.user));

    this.userAuthenticationRest.registerUser(this.user)
      .subscribe(
        user => this.user,
        error =>  this.errorMessage = <any>error,
        () => this.onRegisteredUser()
      );
  }

  private onRegisteredUser(){
    console.log('User registered correctly')

    let toast = this.toastCtrl.create({
      message: 'Utente correttamente registrato: '+ this.user.firstName + ' ' + this.user.lastName + ' , confermare l\'accesso effettuando login con indirizzo email',
      duration: 2000,
      position: 'top',
    });

    toast.onDidDismiss(() => {
        console.log('Dismissed toast utente registrato');

        this.events.publish('user:created', this.user, Date.now());
    });

    toast.present();

  }

}
