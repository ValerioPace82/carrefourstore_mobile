import { Component, ViewChild, ElementRef } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from "@angular/animations";
import { UserAddressManagementProvider } from "../../providers/user-address-management/user-address-management";
import { User } from "../../providers/user-authentication-rest/user-authentication-rest";
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LatLon } from '../../providers/distribution-point-rest/geodesic';
import { DeliveryAddress } from "../../providers/delivery-address-rest/delivery-address-rest";
import { ExternalGeocodingProvider, GeoCodingInfo, GeoCodingLocation, ReverseGeoCoding } from '../../providers/external-geocoding/external-geocoding';
import L from 'leaflet';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';
import moment from'moment';
/**
 * Generated class for the AddDeliveryAddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-delivery-address',
  templateUrl: 'add-delivery-address.html',
  animations: [
        trigger('itemState', [
            transition('void => *', [
                style({transform: 'translateX(-100%)', opacity: 1}),
                animate('600ms ease-out')
            ]),
            transition('* => void', [
                animate('600ms ease-in', style({transform: 'translateX(100%)', opacity: 0}))
            ])
        ])
    ]
})
export class AddDeliveryAddressPage {

  @ViewChild('map-delivery', {read: ElementRef}) mapContainer: ElementRef;
  map: L.Map;

  deliveryAddressForm:FormGroup;
  errorMessage: string;
  latLonSelected: LatLon;
  user: User;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private formBuilder: FormBuilder,
              public externalGeocodingRest: ExternalGeocodingProvider,
              public userAddressManagementRest: UserAddressManagementProvider,
              private storage: Storage,
              private alertCtrl: AlertController) {
    this.user = this.navParams.get('user');

    this.deliveryAddressForm = this.formBuilder.group({
      address: ['', Validators.required],
      city: ['', Validators.required],
      cap: ['', Validators.compose([
    		Validators.required,
    		Validators.pattern('^[0-9]{5}$')
    	])],
    });
  }

  ionViewDidLoad() {
    if(this.map == null || this.map == undefined){
      this.loadmap();
    }
  }

  findGeoCodingInfo(location){

    this.externalGeocodingRest.findGeoCodingInfo(location)
       .subscribe(
         geoCodingInfo => this.reloadMap(geoCodingInfo),
         error =>  this.errorMessage = <any>error);
  }

  locateGeoCodingInfo(lat, lng){

    this.latLonSelected = new LatLon(lat, lng);
    this.externalGeocodingRest.locateGeoCodingInfo(lat, lng)
       .subscribe(
         reverseGeoCoding => this.reloadForm(reverseGeoCoding),
         error =>  this.errorMessage = <any>error);
  }

  addAddress(user, address){
    address.progIndex = user.deliveryAddresses.length;
    user.deliveryAddresses.push(address);
    this.userAddressManagementRest.addAddress(user)
      .subscribe(
        user => this.onUpdateUserAddress(user),
        error =>  this.errorMessage = <any>error);
  }

  onUpdateUserAddress(user){
     console.log('user updated address: ' + JSON.stringify(user));
     if(this.user.userId != -1){
       this.storage.set('user', this.user);
     }
     this.navCtrl.pop();
  }

  loadmap() {

      var LeafIcon = L.Icon.extend({
      options: {
          shadowUrl: 'assets/marker-shadow.png',
          iconSize:     [40, 40],
          shadowSize:   [40, 40],
          iconAnchor:   [22, 95],
          shadowAnchor: [4, 72],
          popupAnchor:  [-3, -36]
      }
     });

     var carrefourIcon = new LeafIcon({iconUrl: 'assets/marker-icon.png'});

     if (this.map != undefined) {
        this.map.off();
        this.map.remove();
     }

     this.map = L.map("map-delivery").fitWorld();
     L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
       attributions: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
       maxZoom: 16
     }).addTo(this.map);

     let self = this;

     this.map.locate({
       setView: true,
       maxZoom: 16
     }).on('locationfound', (e) => {
       let markerGroup = L.featureGroup();
       let marker: any = L.marker([e.latitude, e.longitude], {icon: carrefourIcon}).on('click', () => {
         alert('Marker clicked');
       });

       markerGroup.addLayer(marker);
       this.map.addLayer(markerGroup);
     }).on('locationerror', (err) => {
         alert(err.message);
     }).on('click', function(e) {
        /*
        var popLocation= e.latlng;
        var popup = L.popup()
        .setLatLng(popLocation)
        .setContent('<p>Hello world!<br />This is a nice popup.</p>')
        .openOn(map);
        */
        self.locateGeoCodingInfo(e.latlng.lat, e.latlng.lng);

    });
  }



  reloadForm(reverseGeoCoding : ReverseGeoCoding){

    this.deliveryAddressForm.get('address').setValue(reverseGeoCoding.road)
    this.deliveryAddressForm.get('address').setErrors(null);
    this.deliveryAddressForm.get('city').setValue(reverseGeoCoding.town);
    this.deliveryAddressForm.get('city').setErrors(null);
    this.deliveryAddressForm.get('cap').setValue(reverseGeoCoding.postcode)
    this.deliveryAddressForm.get('cap').setErrors(null);
  }

  reloadMap(geoCodingInfo : GeoCodingInfo){

    console.log("Geolocation found: " + geoCodingInfo + ", reloading map..");

    if (this.map != undefined) {
       this.map.off();
       this.map.remove();
    }

    var LeafIcon = L.Icon.extend({
    options: {
        shadowUrl: 'assets/marker-shadow.png',
        iconSize:     [40, 40],
        shadowSize:   [40, 40],
        iconAnchor:   [22, 95],
        shadowAnchor: [4, 72],
        popupAnchor:  [-3, -36]
    }
   });

   var carrefourIcon = new LeafIcon({iconUrl: 'assets/marker-icon.png'});

   this.map = L.map("map-delivery").fitWorld();
   L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
     attributions: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
     maxZoom: 16
   }).addTo(this.map);


   let markerGroup = L.featureGroup();
   let userMarker: any = null;
   let i = 0;

   for(let location of geoCodingInfo.locations){

     let marker: any = L.marker([location.latLng.lat, location.latLng.lng], {icon: carrefourIcon});
     userMarker = marker;
     markerGroup.addLayer(marker);

     //focus map on first (and only one) result with zoom level 12, simple..
     if(i == 0){
       this.map.setView(new L.LatLng(location.latLng.lat, location.latLng.lng), 12);
     }
   }

   this.map.addLayer(markerGroup);

   let self = this;

   this.map.on('click', function(e) {
      /*
      var popLocation= e.latlng;
      var popup = L.popup()
      .setLatLng(popLocation)
      .setContent('<p>Hello world!<br />This is a nice popup.</p>')
      .openOn(map);
      */
      self.locateGeoCodingInfo(e.latlng.lat, e.latlng.lng);

  });


  }

  locateAddress(event: any){
      let formObj = this.deliveryAddressForm.value;
      let geolocFindStr = this.getGeoLocFindStr(event, formObj);

      console.log("GeoLoc find string: " + geolocFindStr);

      this.findGeoCodingInfo(geolocFindStr);

  }

  private getGeoLocFindStr(event, formObj) :string {

    let geolocFindStr = "";

    if(event.ngControl.name == "city"){
      let cap = (formObj.cap != null ? formObj.cap : "");

      if(cap != ""){
        geolocFindStr += event.ngControl.value + "," + cap;
      }
      else{
        geolocFindStr += event.ngControl.value;
      }
    }

    else if(event.ngControl.name == "cap"){

       let city = (formObj.city != null ? formObj.city : "");

       if(city != ""){
         geolocFindStr += city + "," + event.ngControl.value;
       }
       else{
         geolocFindStr += event.ngControl.value;
       }
    }

    return geolocFindStr;

  }

  addDeliveryAddress() {
     console.log('form submitted object: ' + JSON.stringify(this.deliveryAddressForm.value));

     let alert = this.alertCtrl.create({
        title: 'Confirma indirizzo',
        message: 'Confermare il nuovo indirizzo di spedizione: ' + this.deliveryAddressForm.value.address
                + ' - '  + this.deliveryAddressForm.value.city
                + ' (cap: '+ this.deliveryAddressForm.value.cap + ') ?',
        buttons: [
          {
            text: 'No',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Si',
            handler: () => {
              console.log('Address confirmed');
              alert.dismiss();

              let address = {
                deliveryAddressId: null,
                address: this.deliveryAddressForm.value.address,
                state: "",
                city: this.deliveryAddressForm.value.city,
                region: "",
                district: "",
                cap: this.deliveryAddressForm.value.cap,
                geoLat: this.latLonSelected.lat,
                geoLng: this.latLonSelected.lng,
                defaultAddressFlag: false,
                progIndex: 0
              } as DeliveryAddress;

              this.addAddress(this.user, address);
              return false;
            }
          }
        ]
      });
      alert.present();


  }

}
