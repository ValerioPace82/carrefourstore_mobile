import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicPageModule } from 'ionic-angular';
import { AddDeliveryAddressPage } from './add-delivery-address';

@NgModule({
  declarations: [
    AddDeliveryAddressPage
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicPageModule.forChild(AddDeliveryAddressPage),
  ],
})
export class AddDeliveryAddressPageModule {}
