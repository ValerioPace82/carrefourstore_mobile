import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NearestDistributionPointsPage } from './nearest-distribution-points';

@NgModule({
  declarations: [
    NearestDistributionPointsPage,
  ],
  imports: [
    IonicPageModule.forChild(NearestDistributionPointsPage),
  ],
})
export class NearestDistributionPointsPageModule {}
