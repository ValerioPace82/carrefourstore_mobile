import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { DistributionPointExt, DistributionPoint, DistributionPointRestProvider} from '../../providers/distribution-point-rest/distribution-point-rest';
import { User } from "../../providers/user-authentication-rest/user-authentication-rest";
import { LatLon } from '../../providers/distribution-point-rest/geodesic';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the NearestDistributionPointsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-nearest-distribution-points',
  templateUrl: 'nearest-distribution-points.html',
})
export class NearestDistributionPointsPage {

  distributionPoints: Array<DistributionPointExt>;
  errorMessage: string;
  user: User;
  cap : string;
  userLatLon : LatLon;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public distributionPointsRest: DistributionPointRestProvider,
    private toastCtrl: ToastController,
    private storage: Storage
    ) {

      this.cap = navParams.get("cap");
      this.userLatLon = navParams.get("userLatLon");

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NearestDistributionPointsPage');

    if(this.user == null || this.user == undefined){
      this.storage.get('user').then((user) => {
        if(user != undefined
            && user != null && user.oath2Bearer != ""){

          this.user = user;
        }

      });
    }

    setTimeout(() => {
      this.findNearestDistributionPointsByCap(this.cap, this.userLatLon);
    }, 1000);

  }

  findNearestDistributionPointsByCap(cap, userLatLon) {
  this.distributionPointsRest.findNearestDistributionPointsExtByCap(cap, userLatLon)
     .subscribe(
       distributionPoints => this.distributionPoints = distributionPoints,
       error =>  this.errorMessage = <any>error);
  }

  private setDefaultDistributionPoint(user, distributionPoint){
    console.log("Updating default distribution point");

    user.defaultDistributionPoint = distributionPoint;
    this.distributionPointsRest.setDefaultDistributionPoint(user)
      .subscribe(
        user => this.user,
        error =>  this.errorMessage = <any>error,
        () => this.onUpdateDefaultDistributionPoint()
      );
  }

  onUpdateDefaultDistributionPoint(){

    this.storage.set('user', this.user);
    let toast = this.toastCtrl.create({
      message: 'Punto vendita preferito aggiornato',
      duration: 3000,
      position: 'bottom'
    });

    toast.present();
  }

  selectDefaultDistributionPoint(dp){
    console.log("default distribution point selected: " + JSON.stringify(dp));

    let defaultDp = {
      distributionPointId: dp.distributionPointId,
      name: dp.name,
      address: dp.address,
      geoLat: dp.geoLat,
      geoLng: dp.geoLng,
    } as DistributionPoint;

    this.setDefaultDistributionPoint(this.user, defaultDp);

  }


}
