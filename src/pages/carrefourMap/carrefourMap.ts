import { Component, ViewChild, ElementRef } from '@angular/core';
import { Events, NavController, ToastController  } from 'ionic-angular';
import { NearestDistributionPointsPage } from '../nearest-distribution-points/nearest-distribution-points';
import { DistributionPoint, DistributionPointRestProvider} from '../../providers/distribution-point-rest/distribution-point-rest';
import { User } from "../../providers/user-authentication-rest/user-authentication-rest";
import { LatLon } from '../../providers/distribution-point-rest/geodesic';
import L from 'leaflet';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-carrefourMap',
  templateUrl: 'carrefourMap.html'
})
export class CarrefourMapPage {
  @ViewChild('map', {read: ElementRef}) mapContainer: ElementRef;
  map: L.Map;
  errorMessage:any;
  user: User;
  nearestDistributionPointsPage:any = NearestDistributionPointsPage;
  distributionPoints: Array<DistributionPoint>;
  constructor(public navCtrl: NavController,
              public distributionPointsRest: DistributionPointRestProvider,
             private toastCtrl: ToastController,
             public events: Events,
             private storage: Storage) {

    console.log('Calling CarrefourMapPage constructor, creating new page');
  }

  ionViewDidEnter() {

    if(this.user == null || this.user == undefined){
      this.storage.get('user').then((user) => {
        if(user != undefined
            && user != null && user.oath2Bearer != ""){

          this.user = user;
        }
        else{

          let toast = this.toastCtrl.create({
            message: 'Utente non autenticato, redirezione alla pagina di login',
            duration: 2000,
            position: 'top',
          });

          toast.onDidDismiss(() => {
              console.log('Dismissed toast utente non autenticato');

              this.events.publish('user:requestLogin');
          });

          toast.present();

        }
      });
    }

  setTimeout(() => {
    if(this.map == null || this.map == undefined){
        this.loadmap();
        this.loadMarkers();
    }
  }, 1000);



  }

  ionViewDidLeave() {
    console.log('Carrefour Map leaving');
    if (this.user != null && this.map != undefined) {
       this.map.off();
       this.map.remove();
       this.map = null;
    }
  }

  loadmap() {

    var LeafIcon = L.Icon.extend({
    options: {
        shadowUrl: 'assets/marker-shadow.png',
        iconSize:     [40, 40],
        shadowSize:   [40, 40],
        iconAnchor:   [22, 95],
        shadowAnchor: [4, 72],
        popupAnchor:  [-3, -36]
    }
   });

   var userMarkerIcon = new LeafIcon({iconUrl: 'assets/marker-icon.png'});

   if (this.map != undefined) {
      this.map.off();
      this.map.remove();
   }

    this.map = L.map("map").fitWorld();
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attributions: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
      maxZoom: 16
    }).addTo(this.map);

    let deliveryAddresses = [];
    if(this.user != undefined && this.user != null
        && this.user.deliveryAddresses != null && this.user.deliveryAddresses.length > 0){
      deliveryAddresses = this.user.deliveryAddresses.filter(function(el){
        return el.defaultAddressFlag;
      });
    }

    if(deliveryAddresses.length > 0){
      console.log("default delivery address found: " + JSON.stringify(deliveryAddresses[0]));
      let defaultDeliveryAddress = deliveryAddresses[0];
      this.map.locate({
        setView: true,
        maxZoom: 16
      }).on('locationfound', (e) => {
        let markerGroup = L.featureGroup();
        let marker: any = L.marker([defaultDeliveryAddress.geoLat, defaultDeliveryAddress.geoLng],
          {icon: userMarkerIcon})
          .on('click', () => {
          //  alert('Marker clicked');
          });

        markerGroup.addLayer(marker);
        this.map.addLayer(markerGroup);
      }).on('locationerror', (err) => {
          alert(err.message);
      })
    }
    else{
      this.map.locate({
        setView: true,
        maxZoom: 16
      }).on('locationfound', (e) => {
        let markerGroup = L.featureGroup();
        let marker: any = L.marker([e.latitude, e.longitude], {icon: userMarkerIcon})
        .on('click', () => {
        //  alert('Marker clicked');
        });

        markerGroup.addLayer(marker);
        this.map.addLayer(markerGroup);
      }).on('locationerror', (err) => {
          alert(err.message);
      })
    }
  }

  showDistributionPoints(event){
    // alert('show distribution points please using navCtrl.push showing this view');

    let deliveryAddresses = [];
    if(this.user.deliveryAddresses != null && this.user.deliveryAddresses.length > 0){
      deliveryAddresses = this.user.deliveryAddresses.filter(function(el){
        return el.defaultAddressFlag;
      });
    }

    if(deliveryAddresses.length > 0){
     let defaultDeliveryAddress = deliveryAddresses[0];
     let userLatLon = new LatLon( defaultDeliveryAddress.geoLat, defaultDeliveryAddress.geoLng);
     this.navCtrl.push(this.nearestDistributionPointsPage, {"cap":defaultDeliveryAddress.cap, "userLatLon": userLatLon});
    }
  }

  private setDefaultDistributionPoint(user, distributionPoint){
    console.log("Updating default distribution point");

    user.defaultDistributionPoint = distributionPoint;
    this.distributionPointsRest.setDefaultDistributionPoint(user)
      .subscribe(
        user => this.user,
        error =>  this.errorMessage = <any>error,
        () => this.updateDefaultDistributionPoint()
      );
  }

  private updateDefaultDistributionPoint(){

    console.log("User default distribution point updated");

    this.storage.set('user', this.user);
    let toast = this.toastCtrl.create({
      message: 'Punto vendita preferito aggiornato',
      duration: 3000,
      position: 'bottom'
    });

    toast.present();
  }

  private findNearestDistributionPointsByCap(cap) {
    this.distributionPointsRest.findNearestDistributionPointsByCap(cap)

       .subscribe(
         distributionPoints => this.distributionPoints = distributionPoints,
         error =>  this.errorMessage = <any>error,
         () => this.updateMapMarkers()
       );
  }

  private updateMapMarkers(){

    console.log('#Distribution points loading complete');

    if(this.distributionPoints != null && this.distributionPoints.length > 0){

      let markerGroup = L.featureGroup();
      var CarrefourLeafIcon = L.Icon.extend({
          options: {
              shadowUrl: 'assets/marker-shadow.png',
              iconSize:     [40, 40],
              shadowSize:   [40, 40],
              iconAnchor:   [22, 95],
              shadowAnchor: [4, 72],
              popupAnchor:  [-3, -36]
          }
        });

     var carrefourIcon = new CarrefourLeafIcon({iconUrl: 'assets/carrefour-icon.png'});

      for(let distributionPoint of this.distributionPoints){
        let marker: any = L.marker([distributionPoint.geoLat, distributionPoint.geoLng], {icon: carrefourIcon}).on('click', () => {
          //alert('Supermarket clicked: ' + distributionPoint.name);

          // no need to use event getting distributionPointId, we'll just use object in closure
          // Use the event to find the clicked element
          //var el = $(e.srcElement || e.target),
          //let distributionPointId = el.attr('id');

          this.setDefaultDistributionPoint(this.user, distributionPoint);
        });

        markerGroup.addLayer(marker);
      }

      this.map.addLayer(markerGroup);
      console.log('#Update Distribution points markers');
    }
  }

  loadMarkers(){

    let deliveryAddresses = [];
    if(this.user != undefined && this.user != null
      && this.user.deliveryAddresses != null && this.user.deliveryAddresses.length > 0){
      deliveryAddresses = this.user.deliveryAddresses.filter(function(el){
        return el.defaultAddressFlag;
      });
    }

    if(deliveryAddresses.length > 0){
      let defaultDeliveryAddress = deliveryAddresses[0];
      this.findNearestDistributionPointsByCap(defaultDeliveryAddress.cap);
    }

  }

}
