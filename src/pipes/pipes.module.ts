import { NgModule } from '@angular/core';
import { IconUrlPipe } from './icon-url/icon-url';
import { CategoryIconPipe } from './category-icon/category-icon';
@NgModule({
	declarations: [IconUrlPipe,
    CategoryIconPipe],
	imports: [],
	exports: [IconUrlPipe,
    CategoryIconPipe]
})
export class PipesModule {}
