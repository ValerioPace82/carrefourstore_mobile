import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the CategoryIconPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'categoryIcon',
})
export class CategoryIconPipe implements PipeTransform {
  /**
   * Takes a categoryName and trasnform into ionicicon one.
   */
  transform(value: string, ...args) {

    if(value.toLowerCase().indexOf("acqua") > -1){
      return "water";
    }

    if(value.toLowerCase().indexOf("animali") > -1){
      return "logo-octocat";
    }

    if(value.toLowerCase().indexOf("animali") > -1){
      return "beer";
    }

    if(value.toLowerCase().indexOf("pasta") > -1){
      return "pizza";
    }

    if(value.toLowerCase().indexOf("sughi") > -1){
      return "nutrition";
    }

    return "star";
  }
}
