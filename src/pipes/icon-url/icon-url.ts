import { Pipe, PipeTransform } from '@angular/core';
import { Product } from '../../providers/product-search/product-search';


/**
 * Generated class for the IconUrlPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'iconUrl',
})
export class IconUrlPipe implements PipeTransform {

  private scaledImageApiUrl = 'http://devcarrefourstore.ns0.it:8080/image-server/services/carrefour/product/scaledImage/';

  /**
   * Takes a product and return iconUrl.
   */
  transform(value: Product, ...args) {

    return this.scaledImageApiUrl + value.productId + "?format=png&w=16&h=16";
  }
}
